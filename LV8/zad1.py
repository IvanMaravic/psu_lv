from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from tensorflow.keras.preprocessing import image_dataset_from_directory
import numpy as np
import os
import shutil
testCSV="archive/Test.csv"
testDir="archive/Test_dir"


os.makedirs=open(testDir, exist_ok=True)

rows=open(testCSV).read().strip().split("\n")[1:]

for r in rows:
    (label,imagePath)=r.strip().split(",")[-2:]
    os.makedirs(os.path.join(testDir,label), exist_ok=True)
    shutil.copy(os.path.join("archive",imagePath),os.path.join(testDir,label))

train_ds=image_dataset_from_directory(
    directory='archive/Train',
    labels='inferred',
    label_mode='categorical',
    batch_size=32,
    image_size=(48,48))
test_ds=image_dataset_from_directory(
    directory='archive/Train',
    labels='inferred',
    label_mode='categorical',
    batch_size=1,
    image_size=(48,48))


inputs=keras.Input(shape=(48,48,3))
x=layers.experimental.preprocessing.Rescaling(1./255)(inputs)
x=layers.Conv2d(32,kernel_size=(3,3),padding='same',activation='relu')
x=layers.Conv2d(32,kernel_size=(3,3),padding='valid',activation='relu') #valid --> gubi se po jedan piksel gore-dolje, lijevo-desno
x=layers.MaxPool2D(pool_size = (2,2), strides = (2,2))(x)
x=layers.Dropout(rate = 0.2)(x)
x=layers.Conv2d(64,kernel_size=(3,3),padding='same',activation='relu')
x=layers.Conv2d(64,kernel_size=(3,3),padding='valid',activation='relu')
x=layers.MaxPool2D(pool_size = (2,2), strides = (2,2))(x)
x=layers.Dropout(rate = 0.2)(x)
x=layers.Conv2d(128,kernel_size=(3,3),padding='same',activation='relu')
x=layers.Conv2d(128,kernel_size=(3,3),padding='valid',activation='relu')
x=layers.MaxPool2D(pool_size = (2,2), strides = (2,2))(x)
x=layers.Dropout(rate = 0.2)(x)
outputs=layers.Dense(43, activation='softmax')(x)

model=keras.Model(inputs=inputs,outputs=outputs, name="archive_model")

model.sumary()

#proces treniranja
history=model.complie(loss="categorical_crossentropy",
optimizers="adam",
metrics=["accuracy,"])

#provodjenje ucenja
epochs=2
history=model.evaluate(train_ds,epochs=epochs)

score=model.evaluate(test_ds)
print("Test loss: ",score[0])
print("Test acuracy ",score[1])