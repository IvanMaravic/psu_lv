import matplotlib.pyplot as plt
import numpy as np
import skimage.io
img = skimage.io.imread('tiger.png', as_gray=True)
rows,cols=img.shape
img_new1=np.zeros((cols,rows))
img_new2=np.zeros((rows,cols))
rows3=int(rows/10)
cols3=int(cols/10)
img_new3=np.zeros((rows3,cols3))
img_new4=np.zeros((50,50))
img_new5=255*np.ones((50,50))
img_new6=np.zeros((50,50))
img_new8=255*np.ones((50,50))
img_new9=np.zeros((rows,cols))
for i in range(rows):
    for j in range(cols):
        img[i][j]+=45

for i in range(rows):
    for j in range(cols):
        img_new1[cols-1-j][i]=img[i][j]


for i in range(rows):
    img_new1[:,-i]=img[i,:]

for i in range(rows):
    for j in range(cols):
        img_new2[i][j]=img[i][-j]

for i in range(rows3):
    for j in range(cols3):
        img_new3[i][j]=img[i*10][j*10]

for i in range(rows):
    if i>rows/4 and i<rows/4*2:
        img_new9[i,:]=img[i,:]

        
img_new4 = np.append(img_new4, img_new5,axis=1)
img_new4 = np.append(img_new4, img_new4,axis=1)
img_new4 = np.append(img_new4, img_new6,axis=1)

img_new8 = np.append(img_new5, img_new6,axis=1)
img_new8 = np.append(img_new8, img_new8,axis=1)
img_new8 = np.append(img_new8, img_new5,axis=1)

img_new4 = np.append(img_new4, img_new8,axis=0)
img_new4 = np.append(img_new4, img_new4,axis=0)



plt.figure(1)
plt.imshow(img_new1, cmap='gray', vmin=0, vmax=255)        
plt.figure(2)
plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.figure(3)
plt.imshow(img_new2, cmap='gray', vmin=0, vmax=255)
plt.figure(4)
plt.imshow(img_new3, cmap='gray', vmin=0, vmax=255)
plt.figure(5)
plt.imshow(img_new4, cmap='gray', vmin=0, vmax=255)
plt.figure(6)
plt.imshow(img_new9, cmap='gray', vmin=0, vmax=255)


plt.show()