import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, max_error
from sklearn.preprocessing import MinMaxScaler, StandardScaler

df = pd.read_csv('cars_processed.csv')
print(df.info())

#odabir ulaznih velicina
df=df.drop(['name','mileage','seats',],axis=1)  #izbacivanje imena,.. zato sto ne utjecu na cijenu
df.info

#definira se X i Y
X=df[['km_driven','year','engine','max_power']]
Y=df['selling_price']

#podjela na test i train
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=10)
print(X_train.shape)

#skaliranje podataka
Scaler = MinMaxScaler()
X_train_s = Scaler.fit_transform(X_train)
X_test_s = Scaler.transform(X_test)

#izrada modela
linear_model = LinearRegression()
linear_model.fit(X_train_s, Y_train)

Y_pred_train = linear_model.predict(X_train_s)
Y_pred_test = linear_model.predict(X_test_s)

print("R2 test: ", r2_score(Y_pred_test, Y_test))
print("RMSE test: ", np.sqrt(mean_squared_error(Y_pred_test, Y_test)))
print("Max error test: ", max_error(Y_pred_test, Y_test))
print("MAE test: ", mean_absolute_error(Y_pred_test, Y_test))