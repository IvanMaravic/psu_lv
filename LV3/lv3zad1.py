import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv') 
print(mtcars)

#Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
print(mtcars.sort_values(by=['mpg']).head(5).car) 

#Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
print(mtcars[mtcars.cyl == 8].sort_values(by=['mpg']).tail(3)) 

#Kolika je srednja potrošnja automobila sa 6 cilindara?
print(mtcars[mtcars.cyl == 6].mean().mpg) 

#Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs? 
print(mtcars[(mtcars.cyl == 4) & (mtcars.wt < 2.200) & (mtcars.wt >2.000)].mean().mpg) 

#Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
new_mtcars = mtcars.groupby('am') 
print(new_mtcars.count().car[0])
print(new_mtcars.count().car[1])

#Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga? 
print(len(mtcars[(mtcars.am == 1) & (mtcars.hp > 100)]))

#Kolika je masa svakog automobila u kilogramima? 
print(mtcars.wt*1000*0.45359237)
mtcars['kg']=mtcars['wt']*1000*0.4539237
print(mtcars.kg)


