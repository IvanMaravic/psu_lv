import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv') 
print(mtcars)

mtcars.groupby('cyl')['mpg'].mean().plot.bar()
plt.show()

mtcars.boxplot(by="cyl",column="wt")
plt.show()

mtcars.groupby('am')['mpg'].mean().plot.bar()
plt.show()